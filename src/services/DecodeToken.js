import jwtDecode from 'jwt-decode'

function getFullName() {
    let nom = jwtDecode(getToken()).nom;
    let prenom = jwtDecode(getToken()).prenom;

    return prenom + ' ' + nom;
}

function getToken() {
    return window.localStorage.getItem('authToken');
}

export default {
    getFullName,
}