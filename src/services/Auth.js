import axios from 'axios';
import jwtDecode from 'jwt-decode';

function Authenticate(user) {
    return axios
    .post('http://localhost:8000/api/login_check', user)
    .then(response => response.data.token)
    .then(token => {
        window.localStorage.setItem('authToken', token);
        setAxiosToken(token)
    });
}

function setAxiosToken(token) {
    axios.defaults.headers['Authorization'] = "Bearer "+token;
}

function isAuthenticated() {

    const token = getToken();

    if(token) {
        const jwtData = jwtDecode(token);
        if (jwtData.exp * 1000 > new Date().getTime()) {
            setAxiosToken(token);
            return true
        } else {
            return false;
        }
    } else {
        return false;
    }
    
}

function getToken() {
    return window.localStorage.getItem('authToken');
}

function logout() {
    window.localStorage.removeItem('authToken');
    delete axios.defaults.headers['Authorization'];
}

export default {
    Authenticate,
    isAuthenticated,
    logout
}