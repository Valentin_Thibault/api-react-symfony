import './App.css';
import { Routes, Route } from 'react-router';
import { BrowserRouter } from 'react-router-dom';
import { useState } from 'react'
import Login from './pages/Login';
import Auth from './services/Auth';
import AuthContext from './contexts/AuthContext'
import PrivatePage from './pages/PrivatePage';
import PrivateRoute from './components/PrivateRoute';

function App() {

  const [isAuthenticated, setIsAuthenticated] = useState(Auth.isAuthenticated());


  return (
    <AuthContext.Provider value={{isAuthenticated, setIsAuthenticated}}>
      <BrowserRouter>
        <Routes>
          <Route
            path={"/login"}
            element={<Login />}
          />
          <Route
            path={"/"} 
            element={<PrivateRoute>
              <PrivatePage />
            </PrivateRoute>} 
          />
        </Routes>
      </BrowserRouter>
    </AuthContext.Provider>
  );
}

export default App;
