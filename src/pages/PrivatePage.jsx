import React from 'react'
import { useContext, useEffect } from 'react'
import Auth from '../services/Auth'
import AuthContext from '../contexts/AuthContext'
import { useNavigate } from 'react-router'
import DecodeToken from '../services/DecodeToken'
import axios from 'axios'


const PrivatePage = () => {
    const {setIsAuthenticated} = useContext(AuthContext)
    const navigate = useNavigate()

    const disconnect = () => {
        Auth.logout()
        setIsAuthenticated(false)
        navigate("/login")
    }

    const fetchUsers = async () => {
        try {
            let response = await axios.get('http://localhost:8000/api/users')
            .then(response => response.data).then(data => data['hydra:member'])
            console.log(response);   
        } catch (e) {
            console.log(e);
        }
    }

    useEffect( () => {
        fetchUsers().then(r => '')
    }, [])

    return ( 
        <div>
            {DecodeToken.getFullName()}
            <button onClick={disconnect}>Se déconnecter</button>
        </div>
    );
}
 
export default PrivatePage;