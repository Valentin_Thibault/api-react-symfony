import React, { useContext, useState } from 'react'
import AuthContext from '../contexts/AuthContext'
import Auth from '../services/Auth'
import { useNavigate } from 'react-router'

const Login = () => {
    const [user, setUser] = useState({
        username: "",
        password: ""
    })

    const navigate = useNavigate()
    const {setIsAuthenticated} = useContext(AuthContext)

    const handleChange = ({target}) => {
        let {name, value} = target;
        setUser({...user, [name]: value})
    }

    const handleSubmit = async () => {
        await Auth.Authenticate(user)
        setIsAuthenticated(true)
        navigate("/")
    }

    return (  
        <div>
            <input type="text" name={"username"} id={"username"} onChange={handleChange} value={user.username} />
            <input type="password" name={"password"} id={"password"} onChange={handleChange} value={user.password} />
            <button onClick={handleSubmit}>Se connecter</button>
        </div>
    );
}
 
export default Login;